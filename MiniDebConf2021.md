<!-- wp:paragraph -->
<p>Successfully completed  <a href="https://in2021.mini.debconf.org/" class="ek-link">MiniDebConf</a> India 2021 with <a href="https://wiki.debian.org" class="ek-link">Debian</a> enthusiasts around the world.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Here's an overview of what happened on those day-long activities. More than half of the sessions were in Telugu. Kudos to all the speakers and hacktivists who made the day memorable. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Total number of talks by Swecha Hacktivists: 17<br>Total number of Swecha Hacktivists as speakers: 22<br>Total number of Swecha Hacktivists in video team: 11</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p> </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>For recorded sessions please find them in the <a href="https://meetings-archive.debian.net/pub/debian-meetings/2021/MiniDebConf-India/" class="ek-link">Debian Videos archive</a> or else in <a href="https://peertube.debian.social/videos/watch/playlist/291f353e-0d86-4ae4-a28b-2f657f4055b9?playlistPosition=3&amp;resume=true">DebConf Videos Peertube Playlist</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>You can also find the recorded videos in the below proprietary platforms:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://www.youtube.com/watch?v=3TWohmSiKOc&amp;list=PLR98RTuuvOS7jSJbWaMHAUQP1vqVFPzuS" class="ek-link">Swecha Youtube Channel</a>.<br><a href="http://fb.com/swechafsmi" class="ek-link">Swecha Facebook Page</a><br><a href="http://instagram.com/swechafsmi" class="ek-link">Swecha Instagram Page</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->
