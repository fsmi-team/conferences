# DebConf21
Accepted talks!
## Schedule

| Time | Talks | Speaker |
| ------ | ------ | ------ |
|**Tuesday (24Aug)** |
| 5:30 - 6:15 PM| **డెబియన్ ప్రాజెక్ట్ - అంతర్జాతీయ ఆపరేటింగ్ సిస్టంగా  దాని సౌలబ్యథ మరియు రుపాంతర** |  Gorla Praveen |  
| 6:30 - 7:15 PM  | **బాల స్వేచ్ఛ ప్రాజెక్టు - కంప్యూటర్ ఆధారిత విద్యలో ఒక కొత్త ఒరవడి** | Srichith@reddy25 & satya jonnakuti |
| 7:30 - 7:50 PM | **వాడుక లో డెబియన్ మెడ్! (Debian Med in Use)** | Bhanu Prasad Marri & Shiva Prasad Ganji  |
| 8:00 - 8:20 PM | **Debian in Retro Computers [పాత కంప్యూటర్లలో డెబియన్]** | Kurva Prashanth | 
| 9:30 - 9:50 PM|  **GUI vs CLI in Debian** | Pavan & Dantu Venkata Sai Kamal |
|**Thursday (26Aug)**|
| 5:30 - 6:15 PM | **Mass Campaigns to increase Debian adoption in India** | Naveen Cherukupalle & Srilekha Mopidevi |
| 7:30 - 7:50 PM | **ಟರ್ಮಿನಲ್ನೊಂದಿಗೆ ವಿನೋದ (Fun with Terminal)** |  Kulkarni Pavan |
| 9:30 - 10:15 PM | **Containerization vs Virtualization** | Rajasekhar Ponakala & Mohan Durga Sathish Kantamsetti  |
|**Saturday (28Aug)**|
| 7:00 - 7:20 PM | **Gaming in Debian (Hindi)** | Vamshi Krishna  |
| 7:30 - 7:50 PM | **Debian for Open Science** |  Sridhar Gutam |
